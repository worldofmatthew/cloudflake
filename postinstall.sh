#!/bin/sh

apt update;apt install golang screen git nohup curl -y;mkdir -p ~/go/src;echo "export GOPATH=$HOME/go" >> ~/.bash_profile;source ~/.bash_profile;cd $GOPATH/src;git clone https://git.torproject.org/pluggable-transports/snowflake.git;cd snowflake;cd proxy;go get;go build;nohup ./proxy &