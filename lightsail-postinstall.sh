#!/bin/bash

if [ -e snowflackinstalled.txt ]
then
    cd $GOPATH/src/snowflake/proxy;screen ./proxy &
else
    touch snowflackinstalled.txt;apt update;apt install golang screen git curl -y;mkdir -p ~/go/src;echo "export GOPATH=$HOME/go" >> ~/.bash_profile;source ~/.bash_profile;cd $GOPATH/src;git clone https://git.torproject.org/pluggable-transports/snowflake.git;cd snowflake;cd proxy;go get;go build;screen ./proxy &
fi

sleep $(( ( RANDOM % 10800 )  + 1800 ))

reboot